# Description and usage

* We accept either comma (,), tab (\t), or semicolon (;) as field separators, but only choose one and make sure you do not use that character within the content of the fields (e.g. if you separate the fields with a comma, do not use commas in the sample description).
* Only the first 10 fields are strictly mandatory for starting the submission. Our curation team will contact you in case problems occur.
* You can remove empty optional fields (i.e. columns without any content), but you don't have to.
* Missing values in optional fields should be left empty, do not use an extra notation (e.g. 'NA', '-').
* You can add extra fields (i.e. columns) **at the end** of the template.

# Field descriptions
* MIxS fields are marked as optional, because you can start a submission without them and add them later (if available). We strongly recommend complying to the [MIxS standard](https://press3.mcs.anl.gov/gensc/mixs/) and support our users in achieving compliance.

field name|mandatory|description
:---|:---|:---
sample_title|**yes**| a unique label for your samples, preferably one you can use to map to any other data (e.g. environmental measurements, experimental conditions).
taxon_id|**yes**|the numeric taxon ID according to NCBI Taxonomy ([search or browse] ([ecological metagenomes](https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Undef&id=410657&lvl=3&keep=1&srchmode=1&unlock)), e.g. [ecological metagenomes](https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Undef&id=410657&lvl=3&keep=1&srchmode=1&unlock)).
sample_description|no (recommended)| Sample titles are often short and uninformative. Therefore, **we strongly recommend adding a short sample description** (e.g. 'surface water sample from'). It can be a combination of the most important features of this sample - imagine what information would help you determine if a sample is interesting for you at a glance.
sequencing_platform|**yes**|The full name of the sequencing machine, e.g. "Illumina HiSeq 2000". Have a look of available [platforms](https://ena-docs.readthedocs.io/en/latest/submit/reads/webin-cli.html#platform) and [instruments](https://ena-docs.readthedocs.io/en/latest/submit/reads/webin-cli.html#instrument).
library_strategy|**yes**| e.g. 'AMPLICON' for community analysis with marker genes like 16S rRNA. See the permitted values for [library_strategy](https://ena-docs.readthedocs.io/en/latest/submit/reads/webin-cli.html#strategy).
library_source|**yes**| e.g. "METAGENOMIC" or "METATRANSCRIPTOMIC" for community based analyses. Refer to the list of available library sources in https://ena-docs.readthedocs.io/en/latest/submit/reads/webin-cli.html#source.
library_selection|**yes**| e.g for amplicon studies, use "PCR". Have a look of the options for [library_selection](https://ena-docs.readthedocs.io/en/latest/submit/reads/webin-cli.html#permitted-values-for-library-selection).
library_layout|**yes**| refers to whether the sequence reads are single-end or paired-end (allowed values: "single" or "paired").
nominal_length|**yes (conditional)**| A single number denoting the expected insert size. This field is only mandatory for paired-end sequencing (i.e. the value of the **library_layout** column is ''paired''). See https://www.ebi.ac.uk/fg/annotare/help/seq_lib_spec.html for more information.
forward_read_file_name|**yes**| The complete filename for the forward read as you upload through the input interface or make available through a file-sharing platform.
forward_read_file_checksum|no| You can calculate the checksum of the read files (e.g. using md5sum on linux) and provide them here. We can use the values to check the integrity of the files after the transfer.
reverse_read_file_name|**no (conditional)**| if you have paired-end sequences (*library_layout=paired*); then this field is mandatory.
reverse_read_file_checksum|no|You can calculate the checksum of the read files (e.g. using md5sum on linux) and provide them here. We can use the values to check the integrity of the files after the transfer.
checksum_method|no|If your provide a checksum for any of your files, then it is **highly recommended** to provide the method you used (allowed values/methods: "MD5" or "SHA-256").
**MIxS and optional parameters** | |
investigation type|no | refers to the type of material you sequenced, the value is one of: eukaryote,bacteria_archaea,plasmid,virus,organelle,metagenome,mimarks-survey,mimarks-specimen. **Tip**: the correct value for amplicon studies is 'mimarks-survey'.
environmental package|no | refers to the [MIxS environmental packages](https://genomicsstandardsconsortium.github.io/mixs/#packages) (e.g. 'water', 'sediment').
collection date|no | The time of sampling, either as an instance (single point in time) or interval (for a time frame). Please provide it in [ISO8601](https://en.wikipedia.org/wiki/ISO_8601) format, e.g. '2016-01-18' or e.g. '2016-01-18/2016-01-31' for an interval.
geographic location (latitude)|no | The latitude in decimal degrees (WGS84), e.g. '32.4567'.
geographic location (longitude)|no | The latitude in decimal degrees (WGS84), e.g. '111.0034'.
geographic location (depth)|no | Depth in meters (do not include the unit). This field is mandatory for the MIxS environmental packages: water, soil, and sediment.
geographic location (elevation)|no | Elevation in meters, e.g. if your sample was taken atop a mountain. If you want to express the distance from the surface to the bottom of a water body, use "total depth of water column". This field is mandatory for certain MIxS environmental packages such as soil, sediment and microbial mat biolfilm.
total depth of water column|no | Distance from the water surface to the bottom in meters (do not include the unit).
geographic location (country and/or sea)|no | The country or sea where the sample was taken from. The value **must be** from the [INSDC country list](http://www.insdc.org/country.html).
environment (biome)|no | Biomes are defined based on factors such as plant structures, leaf types, plant spacing, and other factors like climate. Biome should be treated as the descriptor of the broad ecological context of a sample. Examples include: desert, taiga, deciduous woodland, or coral reef.  Please include the [EnvO](http://www.environmentontology.org) biome term in the format 'forest biome [ENVO:01000174]'. [Browse EnvO biome terms at OLS](https://www.ebi.ac.uk/ols/search?q=biome&ontology=envo). If you have more than one term, please add the value in the following format 'ocean biome [ENVO:01000048] \| marine biome [ENVO: 00000447]'. This also applies for the other two environment fields.
environment (material)|no | The environmental material level refers to the material that was displaced by the sample, or material in which a sample was embedded, prior to the sampling event. Environmental material terms are generally mass nouns. Examples include: air, soil, or water. Please include the [EnvO](http://www.environmentontology.org) environmental material term in the format 'sea water [ENVO:00002149]'. [Browse EnvO material terms at OLS](https://www.ebi.ac.uk/ols/ontologies/envo/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FENVO_00010483).
environment (feature)|no | Environmental feature level includes geographic environmental features. Compared to biome, feature is a descriptor of the more local environment. Examples include: harbor, cliff, or lake. Please include the [EnvO](http://www.environmentontology.org) environmental feature term(s) in the format 'microbial community [PCO:1000004]'. [Browse EnvO feature terms at OLS](https://www.ebi.ac.uk/ols/search?q=environmental+feature&groupField=iri&start=0&ontology=envo). 
project name|(conditional)|a short and concise name for your project. This field is mandatory for all MIxS environmental packages.
geographic location (region and locality)|no| a free-text description of the region and locality where the sample was taken. Can be used for more precise definition of the location with terms, which are not part of the INSDC controlled vocabulary.
